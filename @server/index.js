const express = require('express')
const axios = require('axios')
const cors = require('cors')
const app = express()
const port = 4000

app.use(cors())

const apuBasePath = 'https://api.openweathermap.org'

const apiKey = '1c5da32bd6a0d1c4c017b21b49833c7f'

const _instance = axios.create({
  timeout: 30000
})

const getData = (url, params) => {
  return _instance.get(`${apuBasePath}${url}`, {
    params: { ...params, appid: apiKey }
  })
}

const getCoordinatesByLocationName = (locationName) => {
  return getData('/geo/1.0/direct', { limit: 1, q: locationName })
}

const getForecast = (lat, lon) => {
  return getData('/data/2.5/onecall', { lat, lon, exclude: 'minutely,hourly' })
}

app.get('/forecast', async (req, res) => {
  try {
    const locationName = req.query.q?.toString()
    const coordinate = (await getCoordinatesByLocationName(locationName)).data?.[0]
    const result = (await getForecast(coordinate?.lat, coordinate?.lon)).data
    res.status(200).json({ ...coordinate, ...result })
  } catch (error) {
    const status = error?.response?.status
    const message = error?.response?.data?.message ?? 'Cannot find location'
    res.status(status).json({ message })
  }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
