import axios from 'axios'

const apuBasePath = 'http://localhost:4000'

const _instance = axios.create({
  timeout: 30000
})

const getData = <T>(url: string, params?: Record<string, string | number>) => {
  return _instance.get<T>(`${apuBasePath}${url}`, {
    params
  })
}

export const getForecast = (locationName: string) => {
  return getData<Forecast>('/forecast', { q: locationName })
}

export interface Forecast {
  name?: string
  local_names?: { [key: string]: string }
  lat?: number
  lon?: number
  country?: string
  timezone?: string
  timezone_offset?: string
  daily: DailyForecast[]
}

export interface DailyForecast {
  dt?: number
  sunrise?: number
  sunset?: number
  moonrise?: number
  moonset?: number
  moon_phase?: number
  temp?: Temp
  feels_like?: FeelsLike
  pressure?: number
  humidity?: number
  dew_point?: number
  wind_speed?: number
  wind_deg?: number
  wind_gust?: number
  weather?: Weather[]
  clouds?: number
  pop?: number
  rain?: number
  uvi?: number
}

export interface Temp {
  day?: number
  min?: number
  max?: number
  night?: number
  eve?: number
  morn?: number
}

export interface FeelsLike {
  day?: number
  night?: number
  eve?: number
  morn?: number
}

export interface Weather {
  id?: number
  main?: string
  description?: string
  icon?: string
}
