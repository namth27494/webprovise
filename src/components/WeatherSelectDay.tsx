import { useCallback } from 'react'
import styled from 'styled-components'
import { convertKtoC, convertKtoF, getDateFromUnix, getDayFromDate } from '../helpers'
import { DailyForecast } from '../services'
import { WeatherIcon, WeatherMeasurementUnits } from './shared'

interface IWeatherSelectDay {
  dailyForecasts: DailyForecast[]
  selectedDay: number
  onDayChanged: (index: number) => void
  currentUnit: WeatherMeasurementUnits
}

const WeatherSelectDay = ({
  dailyForecasts,
  selectedDay,
  onDayChanged,
  currentUnit
}: IWeatherSelectDay) => {
  const tempDisplay = useCallback(
    (temp: number) => {
      return (
        currentUnit === WeatherMeasurementUnits.CELCIUS ? convertKtoC(temp) : convertKtoF(temp)
      ).toFixed(0)
    },
    [currentUnit]
  )
  return (
    <WeatherSelectDayContainer>
      {dailyForecasts.map((forecast, index) => (
        <div
          className={`item ${index === selectedDay ? 'selected' : ''}`}
          key={index}
          onClick={() => onDayChanged(index)}
        >
          <span className="day">{getDayFromDate(getDateFromUnix(forecast.dt ?? 0))}</span>
          {forecast.weather?.[0]?.icon && <WeatherIcon type={forecast.weather[0].icon} />}
          <div className="temperature">
            <span className="max">{tempDisplay(forecast.temp?.max ?? 0)}°</span>
            <span className="min">{tempDisplay(forecast.temp?.min ?? 0)}°</span>
          </div>
        </div>
      ))}
    </WeatherSelectDayContainer>
  )
}

const WeatherSelectDayContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  border-top: 1px solid #969696;

  .item {
    display: flex;
    flex-direction: column;
    align-items: center;
    flex: 1;
    padding: 20px 16px;
    border-right: 1px solid #969696;
    cursor: pointer;

    &:last-child {
      border-right: none;
    }

    &.selected {
      background-color: #f7f7f7;
      pointer-events: none;
    }

    .day {
      font-weight: bold;
      font-weight: bold;
      font-size: 14px;
      margin-bottom: 12px;
    }

    img {
      width: 100%;
    }

    .temperature {
      display: flex;
      flex-direction: column;
      align-items: center;

      .max {
        font-weight: bold;
        font-size: 18px;
      }

      .min {
        font-size: 14px;
      }
    }
  }
`

export default WeatherSelectDay
