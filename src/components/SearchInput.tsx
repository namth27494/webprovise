import { HTMLAttributes, useEffect, useState } from 'react'
import styled from 'styled-components'
import { useDebounce } from '../hooks'
import { Loading } from './shared'

interface ISearchInput extends HTMLAttributes<HTMLDivElement> {
  isLoading?: boolean
  isClearable?: boolean
  debounceTime?: number
  initValue?: string
  onChanged: (value: string) => void
}

const SearchInput = ({
  isLoading,
  isClearable = true,
  debounceTime = 300,
  initValue,
  onChanged,
  ...props
}: ISearchInput) => {
  const [value, setValue] = useState(initValue ?? '')
  const debouncedValue = useDebounce(value, debounceTime)

  const onClearText = () => {
    setValue('')
  }

  useEffect(() => {
    debouncedValue.length && onChanged(debouncedValue)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedValue])

  useEffect(() => {
    !value && onChanged(value)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  return (
    <SearchInputContainer className={props.className} {...props}>
      <input
        type="text"
        placeholder={props.placeholder ?? ''}
        value={value}
        onChange={(e) => {
          const val = e.target.value
          !isLoading && setValue(val)
        }}
      ></input>
      {isLoading && <Loading className="loading" />}
      {!isLoading && isClearable && value && (
        <span className="clear-icon" onClick={onClearText}>
          X
        </span>
      )}
    </SearchInputContainer>
  )
}

const SearchInputContainer = styled.div`
  width: 100%;
  display: flex;
  position: relative;

  input {
    width: 100%;
    max-height: 44px;
    border: 1px solid rgba(150, 150, 150, 0.3);
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.25);
    border-radius: 4px;
    padding: 10px 20px;
    font-size: 18px;
  }

  .loading,
  .clear-icon {
    position: absolute;
    width: 14px;
    height: 14px;
    top: calc(50% - 10px);
    right: 16px;
  }

  .clear-icon {
    display: block;
    cursor: pointer;
  }
`

export default SearchInput
