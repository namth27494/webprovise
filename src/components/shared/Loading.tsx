import { HTMLAttributes } from 'react'
import styled from 'styled-components'

export const Loading = (props: HTMLAttributes<HTMLDivElement>) => {
  return (
    <LoadingContainer {...props}>
      <div className="icon"></div>
    </LoadingContainer>
  )
}

const LoadingContainer = styled.div`
  width: 14px;
  height: 14px;
  .icon {
    border: 1px solid #000;
    border-radius: 50%;
    border-top-color: transparent;
    width: 100%;
    height: 100%;
    animation: spin infinite 1s linear;
  }

  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`
