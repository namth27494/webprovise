import { useEffect, useState } from 'react'
import styled from 'styled-components'

export enum WeatherMeasurementUnits {
  CELCIUS = 'C',
  FAHRENHEIT = 'F'
}

interface IWeatherMeasurement {
  initUnit?: WeatherMeasurementUnits
  onUnitChanged: (type: WeatherMeasurementUnits) => void
}

export const WeatherMeasurement = ({ initUnit, onUnitChanged }: IWeatherMeasurement) => {
  const [currentMeasure, setCurrentMeasure] = useState<WeatherMeasurementUnits>(
    initUnit ?? WeatherMeasurementUnits.CELCIUS
  )

  const isSelectedClassname = (type: WeatherMeasurementUnits) => {
    return currentMeasure === type ? 'selected' : ''
  }

  const onChanged = (type: WeatherMeasurementUnits) => {
    setCurrentMeasure(type)
  }

  useEffect(() => {
    onUnitChanged(currentMeasure)
  }, [onUnitChanged, currentMeasure])

  return (
    <WeatherMeasureMentContainer>
      <span
        className={isSelectedClassname(WeatherMeasurementUnits.FAHRENHEIT)}
        onClick={() => onChanged(WeatherMeasurementUnits.FAHRENHEIT)}
      >
        F
      </span>{' '}
      /{' '}
      <span
        className={isSelectedClassname(WeatherMeasurementUnits.CELCIUS)}
        onClick={() => onChanged(WeatherMeasurementUnits.CELCIUS)}
      >
        C
      </span>
    </WeatherMeasureMentContainer>
  )
}

const WeatherMeasureMentContainer = styled.div`
  span {
    color: #888888;
    font-weight: bold;
    font-size: 14px;
    cursor: pointer;
    &.selected {
      color: #000000;
      text-decoration: underline;
      cursor: default;
      pointer-events: none;
    }
  }
`
