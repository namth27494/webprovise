import { HTMLAttributes } from 'react'

const iconPath = (type: string) => `https://openweathermap.org/img/wn/${type}.png`

interface IWeatherIcon extends HTMLAttributes<HTMLImageElement> {
  type: string
}

export const WeatherIcon = ({ type, ...props }: IWeatherIcon) => {
  return <img src={iconPath(type)} alt={type} {...props} />
}
