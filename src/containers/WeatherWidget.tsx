import { useCallback, useEffect, useMemo, useState } from 'react'
import styled from 'styled-components'
import SearchInput from '../components/SearchInput'
import WeatherSelectDay from '../components/WeatherSelectDay'
import {
  Loading,
  WeatherIcon,
  WeatherMeasurement,
  WeatherMeasurementUnits
} from '../components/shared'
import { Forecast, getForecast } from '../services'
import {
  convertKtoC,
  convertKtoF,
  degreeToDirection,
  getDateFromUnix,
  getDayFromDate,
  getHour12
} from '../helpers'

const WeatherWidget = () => {
  const [searchText, setSearchText] = useState('')
  const [currentUnit, setCurrentUnit] = useState(WeatherMeasurementUnits.CELCIUS)
  const [forecast, setForecast] = useState<Forecast | null>(null)
  const [currentDay, setCurrentDay] = useState(0)
  const [isLoading, setIsLoading] = useState(false)

  const currentForecast = useMemo(() => forecast?.daily?.[currentDay], [forecast, currentDay])

  const tempDisplay = useCallback(
    (temp: number) => {
      return (
        currentUnit === WeatherMeasurementUnits.CELCIUS ? convertKtoC(temp) : convertKtoF(temp)
      ).toFixed(0)
    },
    [currentUnit]
  )

  const getData = async (locationName: string) => {
    if (locationName) {
      try {
        setIsLoading(true)
        const result = await getForecast(locationName)
        setForecast(result.data)
      } catch (err) {
        console.error(err?.response?.message)
        setForecast(null)
      } finally {
        setIsLoading(false)
      }
    } else {
      setForecast(null)
    }
  }

  useEffect(() => {
    getData(searchText)
  }, [searchText])

  return (
    <WeatherWidgetContainer>
      <SearchInput
        onChanged={(val) => setSearchText(val)}
        placeholder="Search Location ..."
        className="search-input"
        isLoading={isLoading}
      />
      {forecast ? (
        <div className="weather">
          <WeatherInfoContainer>
            <div className="title">
              <h2 className="location">
                {forecast?.name}, {forecast?.country}
              </h2>
              <h3 className="info">
                <span>
                  {getDayFromDate(getDateFromUnix(currentForecast?.dt ?? 0), 'long')}{' '}
                  {getHour12(getDateFromUnix(currentForecast?.dt ?? 0))}
                </span>{' '}
                • <span className="desciption">{currentForecast?.weather?.[0].description}</span>
              </h3>
            </div>
            <div className="row">
              <div className="col">
                <div className="d-flex">
                  <WeatherIcon type="10d" className="weather-icon" />
                  <div className="temperature">
                    <span className="degree">{tempDisplay(currentForecast?.temp?.eve ?? 0)}°</span>
                    <span className="measure">
                      <WeatherMeasurement initUnit={currentUnit} onUnitChanged={setCurrentUnit} />
                    </span>
                  </div>
                </div>
              </div>
              <div className="col">
                {currentForecast?.humidity && <div>Humidity: {currentForecast?.humidity}%</div>}
                {currentForecast?.wind_deg && (
                  <div>
                    Wind: {currentForecast?.wind_gust}{' '}
                    {currentUnit === WeatherMeasurementUnits.CELCIUS ? 'KPH' : 'MPH'}{' '}
                    {degreeToDirection(currentForecast?.wind_deg)}
                  </div>
                )}
              </div>
            </div>
          </WeatherInfoContainer>
          <div className="weather__selection">
            <WeatherSelectDay
              dailyForecasts={forecast.daily}
              selectedDay={currentDay}
              onDayChanged={(index) => setCurrentDay(index)}
              currentUnit={currentUnit}
            />
          </div>
        </div>
      ) : isLoading ? (
        <LoadingContainer>
          <Loading style={{ width: '28px', height: '28px' }} />
        </LoadingContainer>
      ) : searchText ? (
        <NotFoundContainer>
          <img src="/cloud.svg" alt="not-found" />
          <span>We could not find weather information for the location above</span>
        </NotFoundContainer>
      ) : (
        <></>
      )}
    </WeatherWidgetContainer>
  )
}

const WeatherWidgetContainer = styled.div`
  background: white;
  min-width: 679px;
  max-width: 100%;
  padding: 70px 44px;

  .search-input {
    margin-bottom: 8px;
  }

  .weather {
    background: #ffffff;
    border: 1px solid rgba(150, 150, 150, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
    border-radius: 4px;
  }
`

const WeatherInfoContainer = styled.div`
  padding: 20px;

  .title {
    margin-bottom: 20px;
    .location {
      color: #333333;
      font-size: 20px;
      line-height: 23px;
      font-weight: bold;
      margin: 0;
    }
    .info {
      font-size: 14px;
      line-height: 16px;
      color: #666666;
      font-weight: normal;
      margin: 0;

      .desciption {
        text-transform: capitalize;
      }
    }
  }

  .weather-icon {
    margin-right: 12px;
  }

  .temperature {
    display: flex;
    align-items: flex-start;

    .degree {
      font-style: normal;
      font-weight: bold;
      font-size: 44px;
      line-height: 52px;
      margin-right: 6px;
    }

    .measure {
      margin-top: 8px;
    }
  }
`

const LoadingContainer = styled.div`
  background: #ffffff;
  border: 1px solid rgba(150, 150, 150, 0.3);
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 300px;
`

const NotFoundContainer = styled.div`
  background: #ffffff;
  border: 1px solid rgba(150, 150, 150, 0.3);
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 300px;

  span {
    font-size: 18px;
  }
`

export default WeatherWidget
