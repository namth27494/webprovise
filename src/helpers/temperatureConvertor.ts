export const convertKtoF = (kevin: number) => kevin * 1.8 - 459.67

export const convertKtoC = (kevin: number) => kevin - 273.15
