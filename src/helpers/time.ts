const dayMapping: Record<number, { long: string; short: string }> = {
  1: { short: 'Mon', long: 'Monday' },
  2: { short: 'Tue', long: 'Tuesday' },
  3: { short: 'Wed', long: 'Wednesday' },
  4: { short: 'Thu', long: 'Thursday' },
  5: { short: 'Fri', long: 'Friday' },
  6: { short: 'Sat', long: 'Saturday' },
  0: { short: 'Sun', long: 'Sunday' }
}

export const getDateFromUnix = (dt: number) => new Date(dt * 1000)

export const getCurrentTimeInUnix = () => Math.ceil(new Date().getTime() / 1000)

export const getDayFromDate = (date: Date, type: 'long' | 'short' = 'short') =>
  dayMapping[date.getDay()][type]

export const getHour12 = (time: Date) =>
  time.toLocaleString('en-US', { hour: 'numeric', hour12: true })
