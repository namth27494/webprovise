import './assets/style.css'
import styled from 'styled-components'
import WeatherWidget from './containers/WeatherWidget'

function App() {
  return (
    <AppContainer>
      <WeatherWidget />
    </AppContainer>
  )
}

const AppContainer = styled.div`
  background: grey;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`

export default App
