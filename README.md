# WebProvise
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

Server
### `yarn start-server`

Because of CORS when call directly to openweathermap, then I have to build my own server to call api.
Open [http://localhost:4000](http://localhost:4000) to get result from server.

Client
### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Some notes
- Air Quality: not implemented yet
